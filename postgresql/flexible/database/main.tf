###############################################################################
# Required Variables
###############################################################################

variable "name" {
  type        = string
  description = "Name of the database to create."
}

variable "server" {
    type        = string
    description = "ID of the Postgresql flexible server to create this database on."
}

###############################################################################
# Optional Variables
###############################################################################

variable "charset" {
  type        = string
  default     = "UTF8"
  description = "Specifies the Charset for the Postgresql Database."
}

variable "collation" {
  type        = string
  default     = "C"
  description = "Specifies the Collation for the Postgresql Database."
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_postgresql_flexible_server_database" "object" {
  name      = var.name
  charset   = var.charset
  collation = var.collation
  server_id = var.server
}

###############################################################################
# Outputs
###############################################################################

output "server" {
  value = var.server
}

###############################################################################

output "charset" {
  value = var.charset
}

output "collation" {
  value = var.collation
}

###############################################################################

output "id" {
  value = azurerm_postgresql_flexible_server_database.object.id
}

output "name" {
  value = azurerm_postgresql_flexible_server_database.object.name
}

###############################################################################
