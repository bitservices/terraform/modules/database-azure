###############################################################################
# Optional Variables
###############################################################################

variable "subnet_id" {
  type        = string
  default     = null
  description = "The ID of the subnet to which this asset will belong. Must be specified if subnet is created in the same Terraform run. Ignored if 'subnet_lookup' is 'true'."
}

variable "subnet_class" {
  type        = string
  default     = "postgresql"
  description = "Identifier for the target subnet within its VNET."
}

variable "subnet_lookup" {
  type        = bool
  default     = false
  description = "Lookup subnet based on 'subnet_class'. Must be 'true' if 'subnet_id' is not set."
}

###############################################################################
# Locals
###############################################################################

locals {
  subnet_id   = var.subnet_lookup ? data.azurerm_subnet.object[0].id : var.subnet_id
  subnet_name = format("%s-%s", var.vnet, var.subnet_class)
}

###############################################################################
# Data Sources
###############################################################################

data "azurerm_subnet" "object" {
  count                = var.subnet_lookup ? 1 : 0
  name                 = local.subnet_name
  resource_group_name  = var.group
  virtual_network_name = var.vnet
}

###############################################################################
# Outputs
###############################################################################

output "subnet_id" {
  value = local.subnet_id
}

output "subnet_class" {
  value = var.subnet_class
}

output "subnet_lookup" {
  value = var.subnet_lookup
}

###############################################################################
