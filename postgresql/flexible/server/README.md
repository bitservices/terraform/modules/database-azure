<!---------------------------------------------------------------------------->

# postgresql/flexible/server

#### Manage [Azure] Database for [PostgreSQL] flexible servers

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/database/azure//postgresql/flexible/server`**

-------------------------------------------------------------------------------

### Example Usage

```
variable "owner"     { default = "terraform@bitservices.io" }
variable "company"   { default = "BITServices Ltd"          }
variable "location"  { default = "uksouth"                  }
variable "ipv4_cidr" { default = "10.100.0.0/16"            }
variable "ipv6_cidr" { default = "fd37:b175:5b87:9d00::/56" }

module "my_resource_group" {
  source                         = "gitlab.com/bitservices/group/azure//resource"
  name                           = "foobar"
  owner                          = var.owner
  company                        = var.company
  location                       = var.location
  private_dns_postgresql_enabled = true
}

module "my_vnet" {
  source   = "gitlab.com/bitservices/network/azure//vnet"
  name     = "foobar1"
  cidrs    = tolist([ var.ipv6_cidr, var.ipv4_cidr ])
  group    = module.my_resource_group.name
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_postgresql_subnet" {
  source    = "gitlab.com/bitservices/network/azure//subnet"
  vnet      = module.my_vnet.name
  cidrs     = tolist([ cidrsubnet(var.ipv6_cidr, 8, 0), cidrsubnet(var.ipv4_cidr, 1, 0) ])
  class     = "postgresql"
  group     = module.my_resource_group.name
  owner     = var.owner
  company   = var.company
  location  = var.location
  endpoints = [ "Microsoft.Storage" ]

  delegations = [{
    "name"    = "postgresql"
    "service" = {
      "name"    = "Microsoft.DBforPostgreSQL/flexibleServers"
      "actions" = [
        "Microsoft.Network/virtualNetworks/subnets/join/action"
      ]
    }
  }]

  security_group_rules_extra = [{
    "name"                   = "AllowPostgresqlInBound"
    "access"                 = "Allow"
    "protocol"               = "TCP"
    "priority"               = 400
    "direction"              = "Inbound"
    "source_cidr"            = "VirtualNetwork"
    "destination_cidr"       = "VirtualNetwork"
    "source_port_range"      = "*"
    "destination_port_range" = 5432
  }]
}

module "my_postgresql_flexible_server" {
  source    = "gitlab.com/bitservices/database/azure//postgresql/flexible/server"
  name      = "foo-bar"
  vnet      = module.my_vnet.name
  group     = module.my_resource_group.name
  owner     = var.owner
  company   = var.company
  location  = var.location
  subnet_id = var.my_postgresql_subnet.id
}
```

<!---------------------------------------------------------------------------->

[Azure]:      https://azure.microsoft.com/
[PostgreSQL]: https://azure.microsoft.com/services/postgresql/

<!---------------------------------------------------------------------------->
