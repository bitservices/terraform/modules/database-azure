###############################################################################
# Required Variables
###############################################################################

variable "name" {
  type        = string
  description = "The full name of the Azure Database for PostgreSQL flexible server."
}

variable "vnet" {
  type        = string
  description = "The full VNet name this Postgresql server will be built within."
}

variable "group" {
  type        = string
  description = "The full name of the resource group that will contain this resource."
}

variable "owner" {
  type        = string
  description = "Owner of the resource."
}

variable "company" {
  type        = string
  description = "Company the resource belogs to."
}

variable "location" {
  type        = string
  description = "Datacentre location for this Azure Database for PostgreSQL flexible server."
}

###############################################################################
# Optional Variables
###############################################################################

variable "sku" {
  type        = string
  default     = "B_Standard_B1ms"
  description = "The Postgresql server type in the following format: TypeCode_InstanceName. B_Standard_B1ms is burstable with instance type: Standard_B1ms"
}

variable "release" {
  type        = string
  default     = "13"
  description = "The Postgresql version this server will run."
}

variable "username" {
  type        = string
  default     = "terraform"
  description = "The administrator user name."
}

###############################################################################

variable "backup_retention_days" {
  type        = number
  default     = 7
  description = "How many days worth of backups to retain."
}

###############################################################################

variable "ha_mode" {
  type        = string
  default     = "ZoneRedundant"
  description = "The high availability mode for the PostgreSQL Flexible Server. The only possible value is 'ZoneRedundant'."
}

variable "ha_enabled" {
  type        = bool
  default     = false
  description = "Should the Postgresql server be highly available across zones."
}

###############################################################################

variable "storage_size_gb" {
  type        = number
  default     = 32
  description = "The starting storage size for this Postgresql server in gigabytes."
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_postgresql_flexible_server" "object" {
  name                   = var.name
  version                = var.release
  location               = var.location
  sku_name               = var.sku
  storage_mb             = var.storage_size_gb * 1024
  resource_group_name    = var.group
  administrator_login    = var.username
  delegated_subnet_id    = local.subnet_id
  private_dns_zone_id    = data.azurerm_private_dns_zone.object.id
  backup_retention_days  = var.backup_retention_days
  administrator_password = random_password.object.result

  tags = {
    "SKU"          = var.sku
    "Name"         = var.name
    "VNet"         = var.vnet
    "Group"        = var.group
    "Owner"        = var.owner
    "Company"      = var.company
    "Release"      = var.release
    "Location"     = var.location
    "Subscription" = data.azurerm_subscription.current.display_name
  }

  dynamic "high_availability" {
    for_each = var.ha_enabled ? tolist([ var.ha_mode ]) : []

    content {
      mode = high_availability.value
    }
  }

  lifecycle {
    ignore_changes = [
      zone,
      high_availability.0.standby_availability_zone
    ]
  }
}

###############################################################################
# Outputs
###############################################################################

output "vnet" {
  value = var.vnet
}

output "group" {
  value = var.group
}

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

output "location" {
  value = var.location
}

###############################################################################

output "sku" {
  value = var.sku
}

output "release" {
  value = var.release
}

output "username" {
  value = var.username
}

###############################################################################

output "backup_retention_days" {
  value = var.backup_retention_days
}

###############################################################################

output "ha_mode" {
  value = var.ha_mode
}

output "ha_enabled" {
  value = var.ha_enabled
}

###############################################################################

output "storage_size_gb" {
  value = var.storage_size_gb
}

###############################################################################

output "id" {
  value = azurerm_postgresql_flexible_server.object.id
}

output "cmk" {
  value = azurerm_postgresql_flexible_server.object.cmk_enabled
}

output "fqdn" {
  value = azurerm_postgresql_flexible_server.object.fqdn
}

output "name" {
  value = azurerm_postgresql_flexible_server.object.name
}

output "public" {
  value = azurerm_postgresql_flexible_server.object.public_network_access_enabled
}

###############################################################################
