###############################################################################
# Required Variables
###############################################################################

variable "name" {
  type        = string
  description = "The full name of the Azure Database for MySQL flexible server."
}

variable "vnet" {
  type        = string
  description = "The full VNet name this MySQL server will be built within."
}

variable "group" {
  type        = string
  description = "The full name of the resource group that will contain this resource."
}

variable "owner" {
  type        = string
  description = "Owner of the resource."
}

variable "company" {
  type        = string
  description = "Company the resource belogs to."
}

variable "location" {
  type        = string
  description = "Datacentre location for this Azure Database for MySQL flexible server."
}

###############################################################################
# Optional Variables
###############################################################################

variable "sku" {
  type        = string
  default     = "B_Standard_B1s"
  description = "The MySQL server type in the following format: TypeCode_InstanceName. B_Standard_B1ms is burstable with instance type: Standard_B1ms"
}

variable "release" {
  type        = string
  default     = "8.0.21"
  description = "The MySQL version this server will run."
}

variable "username" {
  type        = string
  default     = "terraform"
  description = "The administrator user name."
}

###############################################################################

variable "backup_geo_redundant" {
  type        = bool
  default     = false
  description = "Should geo redundant backup enabled?"
}

variable "backup_retention_days" {
  type        = number
  default     = 7
  description = "How many days worth of backups to retain."
}

###############################################################################

variable "ha_mode" {
  type        = string
  default     = "ZoneRedundant"
  description = "The high availability mode for the MySQL Flexible Server. The only possible value is 'ZoneRedundant'."
}

variable "ha_enabled" {
  type        = bool
  default     = false
  description = "Should the MySQL server be highly available across zones."
}

###############################################################################

variable "storage_iops" {
  type        = number
  default     = 360
  description = "The storage IOPS for this MySQL server. The upper limit of this value depends on 'sku'."
}

variable "storage_size_gb" {
  type        = number
  default     = null
  description = "The starting storage size for this MySQL server in gigabytes. If undefined, storage size is automatically managed by Azure."
}

###############################################################################
# Locals
###############################################################################

locals {
  storage_auto    = local.storage_size_gb == null ? true : false
  storage_size_gb = var.ha_enabled ? null : var.storage_size_gb
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_mysql_flexible_server" "object" {
  name                         = var.name
  version                      = var.release
  location                     = var.location
  sku_name                     = var.sku
  resource_group_name          = var.group
  administrator_login          = var.username
  delegated_subnet_id          = local.subnet_id
  private_dns_zone_id          = data.azurerm_private_dns_zone.object.id
  backup_retention_days        = var.backup_retention_days
  administrator_password       = random_password.object.result
  geo_redundant_backup_enabled = var.backup_geo_redundant

  tags = {
    "SKU"          = var.sku
    "Name"         = var.name
    "VNet"         = var.vnet
    "Group"        = var.group
    "Owner"        = var.owner
    "Company"      = var.company
    "Release"      = var.release
    "Location"     = var.location
    "Subscription" = data.azurerm_subscription.current.display_name
  }

  dynamic "high_availability" {
    for_each = var.ha_enabled ? tolist([var.ha_mode]) : []

    content {
      mode = high_availability.value
    }
  }

  storage {
    iops              = var.storage_iops
    size_gb           = local.storage_size_gb
    auto_grow_enabled = local.storage_auto
  }

  lifecycle {
    ignore_changes = [
      zone,
      high_availability.0.standby_availability_zone
    ]
  }
}

###############################################################################
# Outputs
###############################################################################

output "vnet" {
  value = var.vnet
}

output "group" {
  value = var.group
}

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

output "location" {
  value = var.location
}

###############################################################################

output "sku" {
  value = var.sku
}

output "release" {
  value = var.release
}

output "username" {
  value = var.username
}

###############################################################################

output "backup_geo_redundant" {
  value = var.backup_geo_redundant
}

output "backup_retention_days" {
  value = var.backup_retention_days
}

###############################################################################

output "ha_mode" {
  value = var.ha_mode
}

output "ha_enabled" {
  value = var.ha_enabled
}

###############################################################################

output "storage_auto" {
  value = local.storage_auto
}

output "storage_iops" {
  value = var.storage_iops
}

output "storage_size_gb" {
  value = local.storage_size_gb
}

###############################################################################

output "id" {
  value = azurerm_mysql_flexible_server.object.id
}

output "fqdn" {
  value = azurerm_mysql_flexible_server.object.fqdn
}

output "name" {
  value = azurerm_mysql_flexible_server.object.name
}

output "public" {
  value = azurerm_mysql_flexible_server.object.public_network_access_enabled
}

output "replica_max" {
  value = azurerm_mysql_flexible_server.object.replica_capacity
}

###############################################################################
