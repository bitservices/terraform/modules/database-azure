###############################################################################
# Optional Variables
###############################################################################

variable "private_dns_zone_suffix" {
  type        = string
  default     = "mysql.database.azure.com"
  description = "The suffix of the private DNS zone for MySQL private link connections. The resource group name is appended to this."
}

###############################################################################
# Locals
###############################################################################

locals {
  private_dns_zone_name = format("%s.%s", var.group, var.private_dns_zone_suffix)
}

###############################################################################
# Data Sources
###############################################################################

data "azurerm_private_dns_zone" "object" {
  name                = local.private_dns_zone_name
  resource_group_name = var.group
}

###############################################################################
# Outputs
###############################################################################

output "private_dns_zone_suffix" {
  value = var.private_dns_zone_suffix
}

###############################################################################

output "private_dns_id" {
  value = data.azurerm_private_dns_zone.object.id
}

output "private_dns_name" {
  value = data.azurerm_private_dns_zone.object.name
}

output "private_dns_tags" {
  value = data.azurerm_private_dns_zone.object.tags
}

output "private_dns_record_sets" {
  value = data.azurerm_private_dns_zone.object.number_of_record_sets
}

output "private_dns_max_vnet_links" {
  value = data.azurerm_private_dns_zone.object.max_number_of_virtual_network_links
}

output "private_dns_max_record_sets" {
  value = data.azurerm_private_dns_zone.object.max_number_of_record_sets
}

output "private_dns_max_registered_vnet_links" {
  value = data.azurerm_private_dns_zone.object.max_number_of_virtual_network_links_with_registration
}

###############################################################################
