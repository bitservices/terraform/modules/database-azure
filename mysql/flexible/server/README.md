<!---------------------------------------------------------------------------->

# mysql/flexible/server

#### Manage [Azure] Database for [MySQL] flexible servers

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/database/azure//mysql/flexible/server`**

-------------------------------------------------------------------------------

### Example Usage

```
variable "owner"     { default = "terraform@bitservices.io" }
variable "company"   { default = "BITServices Ltd"          }
variable "location"  { default = "uksouth"                  }
variable "ipv4_cidr" { default = "10.100.0.0/16"            }
variable "ipv6_cidr" { default = "fd37:b175:5b87:9d00::/56" }

locals {
  my_mysql_subnet_ipv4_cidr = cidrsubnet(var.ipv4_cidr, 1, 0)
  my_mysql_subnet_ipv6_cidr = cidrsubnet(var.ipv6_cidr, 8, 0)
}

module "my_resource_group" {
  source                    = "gitlab.com/bitservices/group/azure//resource"
  name                      = "foobar"
  owner                     = var.owner
  company                   = var.company
  location                  = var.location
  private_dns_mysql_enabled = true
}

module "my_vnet" {
  source   = "gitlab.com/bitservices/network/azure//vnet"
  name     = "foobar1"
  cidrs    = tolist([ var.ipv6_cidr, var.ipv4_cidr ])
  group    = module.my_resource_group.name
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_mysql_subnet" {
  source    = "gitlab.com/bitservices/network/azure//subnet"
  vnet      = module.my_vnet.name
  cidrs     = tolist([ local.my_mysql_subnet_ipv6_cidr, local.my_mysql_subnet_ipv4_cidr ])
  class     = "mysql"
  group     = module.my_resource_group.name
  owner     = var.owner
  company   = var.company
  location  = var.location
  endpoints = tolist([ "Microsoft.Storage" ])

  delegations = [{
    "name"    = "mysql"
    "service" = {
      "name"    = "Microsoft.DBforMySQL/flexibleServers"
      "actions" = [
        "Microsoft.Network/virtualNetworks/subnets/join/action"
      ]
    }
  }]

  security_group_rules_extra = [{
    "name"                   = "AllowMySQLClassicInBound"
    "access"                 = "Allow"
    "protocol"               = "TCP"
    "priority"               = 400
    "direction"              = "Inbound"
    "source_cidr"            = "VirtualNetwork"
    "destination_cidr"       = "VirtualNetwork"
    "source_port_range"      = "*"
    "destination_port_range" = "3306"
  },
  {
    "name"                   = "AllowMySQLReplicationClusterIPv6"
    "access"                 = "Allow"
    "protocol"               = "TCP"
    "priority"               = 401
    "direction"              = "Inbound"
    "source_cidr"            = local.my_mysql_subnet_ipv6_cidr
    "destination_cidr"       = local.my_mysql_subnet_ipv6_cidr
    "source_port_range"      = "*"
    "destination_port_range" = "33061"
  },
  {
    "name"                   = "AllowMySQLReplicationClusterIPv4"
    "access"                 = "Allow"
    "protocol"               = "TCP"
    "priority"               = 402
    "direction"              = "Inbound"
    "source_cidr"            = local.my_mysql_subnet_ipv4_cidr
    "destination_cidr"       = local.my_mysql_subnet_ipv4_cidr
    "source_port_range"      = "*"
    "destination_port_range" = "33061"
  }]
}

module "my_mysql_flexible_server" {
  source    = "gitlab.com/bitservices/database/azure//mysql/flexible/server"
  name      = "foo-bar"
  vnet      = module.my_vnet.name
  group     = module.my_resource_group.name
  owner     = var.owner
  company   = var.company
  location  = var.location
  subnet_id = module.my_mysql_subnet.id
}
```

<!---------------------------------------------------------------------------->

[Azure]: https://azure.microsoft.com/
[MySQL]: https://azure.microsoft.com/services/mysql/

<!---------------------------------------------------------------------------->
