<!---------------------------------------------------------------------------->

# database (azure)

<!---------------------------------------------------------------------------->

## Description

Manage database services on [Azure] such as [Azure] Database for [MariaDB]
and [PostgreSQL].

<!---------------------------------------------------------------------------->

## Modules

* [mariadb/database](mariadb/database/README.md) - Manage [Azure] Database for [MariaDB] databases.
* [mariadb/server](mariadb/server/README.md) - Manage [Azure] Database for [MariaDB] servers.
* [mysql/flexible/database](mysql/flexible/database/README.md) - Manage [Azure] Database for [MySQL] flexible databases.
* [mysql/flexible/server](mysql/flexible/server/README.md) - Manage [Azure] Database for [MySQL] flexible servers.
* [postgresql/flexible/database](postgresql/flexible/database/README.md) - Manage [Azure] Database for [PostgreSQL] flexible databases.
* [postgresql/flexible/server](postgresql/flexible/server/README.md) - Manage [Azure] Database for [PostgreSQL] flexible servers.

<!---------------------------------------------------------------------------->

[Azure]:      https://azure.microsoft.com/
[MySQL]:      https://azure.microsoft.com/services/mysql/
[MariaDB]:    https://azure.microsoft.com/services/mariadb/
[PostgreSQL]: https://azure.microsoft.com/services/postgresql/

<!---------------------------------------------------------------------------->
