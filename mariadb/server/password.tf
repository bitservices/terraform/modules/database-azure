###############################################################################
# Resource - random_password
###############################################################################

resource "random_password" "object" {
  length  = 64
  special = false

  keepers = {
    "database_server_name"         = var.name
    "database_server_location"     = var.location
    "database_server_username"     = var.username
    "database_server_subscription" = data.azurerm_subscription.current.id
  }
}

###############################################################################
# Outputs
###############################################################################

output "password" {
  sensitive = true
  value     = random_password.object.result
}

###############################################################################
