<!---------------------------------------------------------------------------->

# mariadb/server

#### Manage [Azure] Database for [MariaDB] servers

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/database/azure//mariadb/server`**

-------------------------------------------------------------------------------

### Example Usage

```
variable "owner"    { default = "terraform@bitservices.io" }
variable "company"  { default = "BITServices Ltd"          }
variable "location" { default = "uksouth"                  }

module "my_resource_group" {
  source   = "gitlab.com/bitservices/group/azure//resource"
  name     = "foobar"
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_mariadb_server" {
  source   = "gitlab.com/bitservices/database/azure//mariadb/server"
  name     = "foo-bar"
  group    = module.my_resource_group.name
  owner    = var.owner
  company  = var.company
  location = var.location
}
```

<!---------------------------------------------------------------------------->

[Azure]:   https://azure.microsoft.com/
[MariaDB]: https://azure.microsoft.com/services/mariadb/

<!---------------------------------------------------------------------------->
