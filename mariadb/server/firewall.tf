###############################################################################
# Optional Variables
###############################################################################

variable "firewall_ipv4_addresses" {
  type = list(object({
    name          = string
    end_address   = string
    start_address = string
  }))
  default     = []
  description = "List of public IPv4 addresses that can access this database instance across the internet."
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_mariadb_firewall_rule" "ipv4" {
  count               = length(var.firewall_ipv4_addresses)
  name                = var.firewall_ipv4_addresses[count.index].name
  server_name         = azurerm_mariadb_server.object.name
  end_ip_address      = var.firewall_ipv4_addresses[count.index].end_address
  start_ip_address    = var.firewall_ipv4_addresses[count.index].start_address
  resource_group_name = var.group
}

###############################################################################
# Outputs
###############################################################################

output "firewall_ipv4_addresses" {
  value = var.firewall_ipv4_addresses
}

###############################################################################

output "firewall_ipv4_ids" {
  value = azurerm_mariadb_firewall_rule.ipv4.*.id
}

###############################################################################
