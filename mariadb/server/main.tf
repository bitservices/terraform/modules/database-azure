###############################################################################
# Required Variables
###############################################################################

variable "name" {
  type        = string
  description = "The full name of the Azure Database for MariaDB server."
}

variable "group" {
  type        = string
  description = "The full name of the resource group that will contain this resource."
}

variable "owner" {
  type        = string
  description = "Owner of the resource."
}

variable "company" {
  type        = string
  description = "Company the resource belogs to."
}

variable "location" {
  type        = string
  description = "Datacentre location for this Azure Database for MariaDB server."
}

###############################################################################
# Optional Variables
###############################################################################

variable "sku" {
  type        = string
  default     = "B_Gen5_1"
  description = "The MariaDB server type in the following format: TypeCode_GenCode_Cores. B_Gen5_1 is Basic, generation 5 with 1 core."
}

variable "tls" {
  type        = bool
  default     = true
  description = "Permit only TLS connections to this MariaDB server? 'false' will permit both TLS and plain connections."
}

variable "release" {
  type        = string
  default     = "10.3"
  description = "The MariaDB version this server will run."
}

variable "username" {
  type        = string
  default     = "terraform"
  description = "The administrator user name."
}

###############################################################################

variable "storage_size_gb" {
  type        = number
  default     = 20
  description = "The starting storage size for this MariaDB server in gigabytes."
}

variable "storage_auto_grow" {
  type        = bool
  default     = false
  description = "Should storage automatically grow if it gets full to avoid downtime?"
}

###############################################################################

variable "backup_geo_redundant" {
  type        = bool
  default     = false
  description = "Should backups have redundant copies stored in another region."
}

variable "backup_retention_days" {
  type        = number
  default     = 7
  description = "How many days worth of backups to retain."
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_mariadb_server" "object" {
  name                         = var.name
  version                      = var.release
  location                     = var.location
  sku_name                     = var.sku
  storage_mb                   = var.storage_size_gb * 1024
  auto_grow_enabled            = var.storage_auto_grow
  resource_group_name          = var.group
  administrator_login          = var.username
  backup_retention_days        = var.backup_retention_days
  ssl_enforcement_enabled      = var.tls
  administrator_login_password = random_password.object.result
  geo_redundant_backup_enabled = var.backup_geo_redundant

  tags = {
    "SKU"          = var.sku
    "TLS"          = var.tls
    "Name"         = var.name
    "Group"        = var.group
    "Owner"        = var.owner
    "Company"      = var.company
    "Release"      = var.release
    "Location"     = var.location
    "Subscription" = data.azurerm_subscription.current.display_name
  }
}

###############################################################################
# Outputs
###############################################################################

output "group" {
  value = var.group
}

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

output "location" {
  value = var.location
}

###############################################################################

output "sku" {
  value = var.sku
}

output "tls" {
  value = var.tls
}

output "release" {
  value = var.release
}

output "username" {
  value = var.username
}

###############################################################################

output "storage_size_gb" {
  value = var.storage_size_gb
}

output "storage_auto_grow" {
  value = var.storage_auto_grow
}

###############################################################################

output "backup_geo_redundant" {
  value = var.backup_geo_redundant
}

output "backup_retention_days" {
  value = var.backup_retention_days
}

###############################################################################

output "id" {
  value = azurerm_mariadb_server.object.id
}

output "fqdn" {
  value = azurerm_mariadb_server.object.fqdn
}

output "name" {
  value = azurerm_mariadb_server.object.name
}

###############################################################################
