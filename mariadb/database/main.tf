###############################################################################
# Required Variables
###############################################################################

variable "name" {
  type        = string
  description = "Name of the database to create."
}

variable "group" {
  type        = string
  description = "The full name of the resource group that will contain this resource."
}

variable "server" {
    type        = string
    description = "Name of the MariaDB server to create this database on."
}

###############################################################################
# Optional Variables
###############################################################################

variable "charset" {
  type        = string
  default     = "utf8"
  description = "Specifies the Charset for the MariaDB Database."
}

variable "collation" {
  type        = string
  default     = "utf8_general_ci"
  description = "Specifies the Collation for the MariaDB Database."
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_mariadb_database" "object" {
  name                = var.name
  charset             = var.charset
  collation           = var.collation
  server_name         = var.server
  resource_group_name = var.group
}

###############################################################################
# Outputs
###############################################################################

output "group" {
  value = var.group
}

output "server" {
  value = var.server
}

###############################################################################

output "charset" {
  value = var.charset
}

output "collation" {
  value = var.collation
}

###############################################################################

output "id" {
  value = azurerm_mariadb_database.object.id
}

output "name" {
  value = azurerm_mariadb_database.object.name
}

###############################################################################
