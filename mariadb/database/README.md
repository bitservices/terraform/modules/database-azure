<!---------------------------------------------------------------------------->

# mariadb/database

#### Manage [Azure] Database for [MariaDB] databases

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/database/azure//mariadb/database`**

-------------------------------------------------------------------------------

### Example Usage

```
variable "owner"    { default = "terraform@bitservices.io" }
variable "company"  { default = "BITServices Ltd"          }
variable "location" { default = "uksouth"                  }

module "my_resource_group" {
  source   = "gitlab.com/bitservices/group/azure//resource"
  name     = "foobar"
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_mariadb_server" {
  source   = "gitlab.com/bitservices/database/azure//mariadb/server"
  name     = "foo-bar"
  group    = module.my_resource_group.name
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_mariadb_database" {
  source = "gitlab.com/bitservices/database/azure//mariadb/database"
  name   = "foo"
  group  = module.my_resource_group.name
  server = module.my_mariadb_server.name
}
```

<!---------------------------------------------------------------------------->

[Azure]:   https://azure.microsoft.com/
[MariaDB]: https://azure.microsoft.com/services/mariadb/

<!---------------------------------------------------------------------------->
